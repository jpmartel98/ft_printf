/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jean-phil <jemartel@student.42quebec>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/15 10:36:18 by jean-phil         #+#    #+#             */
/*   Updated: 2021/07/19 14:25:20 by jean-phil        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printhex(unsigned int input, int type, int len)
{
	unsigned int	nbr;

	nbr = (unsigned int) input;
	if (nbr >= 16)
	{
		len += ft_printhex(nbr / 16, type, len);
		return (len += ft_put(ft_gethexa(nbr, type)));
	}
	else
		return (len += ft_put(ft_gethexa(nbr, type)));
}

int	ft_converter(va_list lst, char type, int len)
{
	int	temp;

	if (type == 's')
		len += ft_putstr(va_arg(lst, char *));
	else if (type == 'p')
		len += ft_printptr(va_arg(lst, void *));
	else if (type == 'd' || type == 'i')
	{
		 temp = va_arg(lst, int);
		 if (temp < 0)
			 len++;
		len += ft_putnb(temp, 1, 1);
	}
	else if (type == 'x')
		len += ft_printhex(va_arg(lst, int), 1, 0);
	else if (type == 'X')
		len += ft_printhex(va_arg(lst, unsigned int), 2, 0);
	else if (type == 'c')
		len += ft_put(va_arg(lst, int));
	else if (type == 'u')
		len += ft_putnb(va_arg(lst, unsigned int), 1, -1);
	else if (type == '%')
		len += ft_put('%');
	return (len);
}

int	ft_printf(const char *str, ...)
{
	int			len;
	int			inc;
	va_list		lst;

	len = 0;
	inc = 0;
	if (!str)
		return (0);
	va_start(lst, str);
	while (*str)
	{
		if (*str == '%')
		{
			str++;
			 len = ft_converter(lst, *str, len);
		}
		else
			inc += ft_put(*str);
		str++;
	}
	va_end(lst);
	return (len + inc);
}
