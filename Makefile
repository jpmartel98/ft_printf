CC = gcc
CFLAGS += -Wall -Wextra -Werror
NAME= libftprintf.a
FILES =  ft_printf.c  ft_printf_utils.c 
OBJS = $(FILES:.c=.o)
HEAD = ft_printf.h

all: $(NAME)

$(NAME): $(OBJS)
	@gcc $(FLAGS) -c $(FILES) -I$(HEAD) 
	@ar -rc $(NAME) $(OBJS)
	@ranlib $(NAME) 

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean 
