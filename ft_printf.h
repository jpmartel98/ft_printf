/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jean-phil <jemartel@student.42quebec>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/14 23:18:02 by jean-phil         #+#    #+#             */
/*   Updated: 2021/07/19 14:26:29 by jean-phil        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef  FT_PRINTF_H

# define  FT_PRINTF_H
# include <stdlib.h>
# include <unistd.h>
# include "stdio.h"
# include "stdlib.h"
# include "limits.h"
# include <stdarg.h>
int		ft_putstr(char *str);
int		ft_put(char carac);
char	ft_gethexa(int elem, int type);
int		ft_putnb(int input, int len, int type);
int		ft_printptr(void *stuff);
int		ft_printhex(unsigned int input, int type, int len);
int		ft_converter(va_list lst, char type, int len);
int		ft_printf(const char *str, ...);
#endif 
