/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jean-phil <jemartel@student.42quebec>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/15 10:35:04 by jean-phil         #+#    #+#             */
/*   Updated: 2021/07/22 21:46:56 by jean-phil        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_putstr(char *str)
{
	int	i;

	i = 0;
	if (!str)
		return (write(1, "(null)", 6));
	while (str[i])
	{
		write(1, &str[i], 1);
		i++;
	}
	return (i);
}

int	ft_put(char input)
{
	return (write(1, &input, 1));
}

char	ft_gethexa(int stuff, int type)
{
	const char		*str = "0123456789abcdef";
	const char		*str2 = "0123456789ABCDEF";
	unsigned int	nice;

	nice = stuff;
	if (type == 0 || type == 1)
		return (str[nice % 16]);
	else
		return (str2[nice % 16]);
}

int	ft_putnb( int input, int len, int type)
{
	unsigned int	nbr;

	if (input < 0 && type != -1)
	{
		ft_put('-');
		nbr = (unsigned int)(-1 * input);
	}
	else
		nbr = input;
	if (nbr >= 10)
	{
		len += ft_putnb(nbr / 10, len, type);
		return (ft_putnb(nbr % 10, len++, type));
	}
	else
	{
		ft_put(nbr + '0');
		return (len);
	}
}

int	ft_printptr(void *stuff)
{
	char				midlayer[17];
	int					inc;
	int					cin;
	size_t				swap;

	inc = 0;
	cin = 0;
	swap = (size_t) stuff;
	cin = write(1, "0x", 2);
	if (swap == 0)
		cin += write(1, "0", 1);
	while (swap)
	{
		midlayer[++inc] = ft_gethexa(swap, 0);
		swap /= 16;
	}
	while (inc)
		cin += ft_put(midlayer[inc--]);
	return (cin);
}
